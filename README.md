# Fastapi Microservices

Simple microservices example consisting of 3 services and a Redis database. The main purpose of this project is to present a concept of microservices on non-complicated example.

## Getting Started

To run the app simply start docker-compose in main directory by entering command below:

```
docker-compose up
```

If you prefer to start services in the background then execute this command: 

```
docker-compose up -d
```

To tear down application execute this command:

```
docker-compose down
```

### Overview

The application consists of three basic services:

* React App Frontend
* FastAPI Inventory Microservice
* FastAPI Payment Microservice

The latter two services are the only ones communicating directly with a Redis Database which will be run using docker-compose.

### Installation

A step by step guide that will tell you how to get the development environment up and running.

```
$ First step
$ Another step
$ Final step
```

## Usage

A few examples of useful commands and/or tasks.

```
$ First example
$ Second example
$ And keep this in mind
```

## Deployment

Technically the application is production ready.

### Server

* Live:
* Release:
* Development:

### Branches

* Master:
* Feature:
* Bugfix:
* etc...

## Additional Documentation and Acknowledgments

* FastAPI Documentation: https://fastapi.tiangolo.com/
* React App Documentation: https://create-react-app.dev/docs
* NodeJS Documentation: https://nodejs.org/en/docs/
* Docker Documentation: https://docs.docker.com/
